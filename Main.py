import os, fnmatch
from tkinter import *
from tkinter import filedialog
from tkinter import messagebox
from tkinter import ttk
from tkinter.scrolledtext import ScrolledText
from buscaRemplaza import *
from listas import *
import json

def browse_button():
    # Allow user to select a directory and store it in global var
    # called folder_path
    global folder_path
    filename = filedialog.askdirectory()
    folder_path.set(filename)
    
def buscaRemplaza():
    coincidence = valorActual.get()
    modification = nuevoValor.get()
    modifications={
        attrBuscar.get():modification.strip()
    }
    directorio = folder_path.get()
    tipoArchivos = tipo_archivo.get()
    archivosModificados = 0

    try:
        for path, dirs, files in os.walk(os.path.abspath(directorio)):
            for filename in fnmatch.filter(files, tipoArchivos):
                filepath = os.path.join(path, filename)
                s = openFile(filepath)
                if tagBuscar.get() != "---":
                    coincidences = json.loads(attr.get(1.0,END))
                    modifications = json.loads(e2.get(1.0, END))
                    s = updateTag(s, tagBuscar.get(), coincidences, modifications)
                
                if tagCrear.get() != "---":
                    nuevoTag = tagCrear.get()
                    nuevoTagAttr = json.loads(nTagAttr.get(1.0,END)) #Atributos del nuevo tag
                    #anxarAlTagRequerimientos = json.loads(appendAttr.get(1.0,END)) #Atributos que coinciden con el tag donde se anexara el nuevo tag
                    nuevoTag = createTag(s, nuevoTag, nuevoTagAttr)
                    if(anexarAlTag.get() != "---"):
                        anexoTag = anexarAlTag.get()
                        s = appendTag(s, anexoTag, nuevoTag)
                closeFile(filepath, s)
                archivosModificados += 1
        if archivosModificados > 0:
            if archivosModificados > 1:
                messagebox.showinfo("Exito!","Se modificaron "+str(archivosModificados)+" archivos exitosamente!!")
            else:
                messagebox.showinfo("Exito!","Se modifico "+str(archivosModificados)+" archivo exitosamente!!")
        else:
            messagebox.showinfo("info","No se modifico ningun archivo")

    except:
        messagebox.showinfo("Error!",sys.exc_info()[1])


def validaForm():
    if not folder_path.get():
        messagebox.showinfo("Error!", "Se debe seleccionar la carpeta donde se efectuaran los cambios")
        return
    if ((not tagBuscar.get() != "---")  and (not tagCrear.get() != "---")):
        messagebox.showinfo("Error!", "Debe seleccionar si desea modificar un tag, agregar un tag o ambas operaciones")
        return
    if(tagBuscar.get() != "---" and (attr.compare("end-1c", "==", "1.0") or e2.compare("end-1c", "==", "1.0"))):
        messagebox.showinfo("Error!", "Se debe asignar al menos los atributos del tag a buscar")
        return
    if(tagCrear.get() != "---" and anexarAlTag.get() == "---"):
        messagebox.showinfo("Error!", "Se debe asignar el tag de destino")
        return
    buscaRemplaza()


window = Tk()
folder_path = StringVar()       #Ruta de carpeta
tipo_archivo = StringVar()      #Tipo de archivo
tagBuscar = StringVar()         #Buscar tags tipo
attrBuscar = StringVar()        #con el atributo...
valorActual = StringVar()       #que tenga el valor...
nuevoValor = StringVar()        #cambiarlo por este valor
tagCrear = StringVar()          #Crear nuevo tag
anexarAlTag = StringVar()       #Anexarlo al tag

window.geometry('750x600')
window.title("Actualizar archivos HTML | JSP")

lblSelectFolder = Label(master=window,text="Selecione la carpeta de busqueda")
lblSelectFolder.grid(row = 0, column = 0, sticky = W, pady = 20, padx = 10)
lblFolderSelected = Label(master=window,textvariable=folder_path)
lblFolderSelected.grid(row = 0, column = 2, sticky = W, pady = 20)
btn = Button(window, text="Examinar", command=browse_button, width=20)
btn.grid(column=1, row=0, sticky = W, pady = 7)
lblArchivos = Label(master=window,text="Seleccionar el tipo de archivo")
lblArchivos.grid(row = 1, column = 0, sticky = W, pady = 2, padx = 10)

w=ttk.Combobox(window, textvariable=tipo_archivo, values=ARCHIVOS,width=20)
w.grid(column=1, row=1, sticky = W, pady = 7)
w.current(0)
lblTagBuscar = Label(master=window,text="Buscar los Tags tipo: ")
lblTagBuscar.grid(row = 2, column = 0, sticky = W, pady = 2, padx = 10)
tag=ttk.Combobox(window, textvariable=tagBuscar, values=TAGS,width=20)
tag.grid(column=1, row=2, sticky = W, pady = 7)
tag.current(0)

lblAttrBuscar = Label(master=window,text="con el/los siguente(s) attributo(s):\nDejar vacio en caso de no agregar attributos", justify=LEFT)
lblAttrBuscar.grid(row = 3, column = 0, sticky = W, pady = 2, padx = 10)
attr =  ScrolledText(window, width=25, height=4)
attr.grid(column=1, row=3, sticky = W, pady = 7)
lblAttrBuscarEjem = Message(master=window, fg="gray", text="{ \n   \"src\":\"/img/imagen.jpg\",\n   \"alt\":\"Nombre de imagen\"\n  }", width=200)
lblAttrBuscarEjem.grid(column=2, row=3, sticky = W)

lblChange = Label(master=window,text="y cambiarlo(s) por estos:", justify=LEFT, padx = 10)
lblChange.grid(row = 4, column = 0, sticky = W, pady = 2, padx = 10)
e2 =  ScrolledText(window, width=25, height=4)
e2.grid(column=1, row=4, sticky = W, pady = 7)
lblChangeEjem = Message(master=window, fg="gray", text="{ \n   \"src\":\"/img/nueva_imagen.jpg\",\n   \"alt\":\"Nombre de nueva imagen\"\n  }", width=200)
lblChangeEjem.grid(column=2, row=4, sticky = W)

lblCreaTag = Label(master=window,text="y/o crear Tag: ")
lblCreaTag.grid(row = 6, column = 0, sticky = W, pady = 2, padx = 10)
ntag=ttk.Combobox(window, textvariable=tagCrear, values=TAGS_CREAR, width=20)
ntag.grid(column=1, row = 6, sticky = W, pady = 7)
ntag.current(0)

lblCreaTagAttr = Label(master=window,text="con los siguentes attributos:\nDejar vacio en caso de no agregar attributos", justify=LEFT)
lblCreaTagAttr.grid(row = 7, column = 0, sticky = W, pady = 2, padx = 10)
nTagAttr =  ScrolledText(window,width=25, height=4)
nTagAttr.grid(column=1, row=7, sticky = W, pady = 7)
lblEjem = Message(master=window, fg="gray", text="{ \n   \"href\":\"localhost:8080/linktothing\",\n   \"onclick\":\"javascript:funtion()\"\n  }", width=200)
lblEjem.grid(column=2, row=7, sticky = W)

lblAppendTag = Label(master=window,text="y anexarlo al tag: ")
lblAppendTag.grid(row = 8, column = 0, sticky = W, pady = 2, padx = 10)
appTag=ttk.Combobox(window, textvariable=anexarAlTag, values=TAGS_CREAR_ANEX, width=20)
appTag.grid(column=1, row = 8, sticky = W, pady = 7)
appTag.current(0)

# lblAppendTag = Label(master=window,text="que tenga los siguientes atributos:\nDejar vacio en caso de no agregar attributos", justify=LEFT)
# lblAppendTag.grid(row = 9, column = 0, sticky = W, pady = 2, padx = 10)
# appendAttr =  ScrolledText(window,width=25, height=4)
# appendAttr.grid(column=1, row=9, sticky = W, pady = 7)
# lblEjem1 = Message(master=window, fg="gray", text="{ \n   \"id\":\"contenedor\",\n   \"class\":\"container\"\n  }", width=200)
# lblEjem1.grid(column=2, row=9, sticky = W)

b = Button(master=window, text="Vamos!", command=validaForm, width=20)
b.grid(row = 15, column = 1, sticky = N, pady = 20)

window.mainloop()