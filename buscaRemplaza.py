import re
import codecs
from bs4 import BeautifulSoup

def updateTag(document, tag, coincidence, modifications):
    tags = document.find_all(tag)
    for child in tags:
        for attr in coincidence:
            if child.has_attr(attr):
                found = re.search(coincidence[attr], child[attr], re.IGNORECASE)
                if(found):
                    for key, value in modifications.items():
                        child[key] = value
    return document

def createTag(document, tag, atributos):
    tag = document.new_tag(tag)
    for attr in atributos:
        tag[attr] = atributos[attr]
    return tag

def appendTag(document, destinationTag, newTag):
    tag = document.find(destinationTag)
    tag.append(newTag)
    return document

def openFile(directory):
    with open(directory) as f:
        s = f.read()
        document = BeautifulSoup(s, "html5lib")
        return document

def closeFile(directory, document):
    soup_string = str(document)
    with open(directory, "w") as f:
        f.write(soup_string)
